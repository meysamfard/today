package now.time.today;


import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class aAsyncTask extends AsyncTask<String, Void, String> {

    public interface AsyncResponse {
        void processFinish(String output);
    }

    URL url;
    HttpURLConnection conn;
    public AsyncResponse delegate = null;

    String q, url_str, responsetxt;

    public aAsyncTask(String urlstr, String str, AsyncResponse delegatee) {
        q = str;
        url_str = urlstr;
        delegate = delegatee;
    }

    @Override
    protected String doInBackground(String... strings) {
        //Log.e("UPDATE", "1");

        try {

            url = new URL(url_str);


            //String param = "q=" + URLEncoder.encode(q, "UTF-8");
            String param = q;

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(30000); //30 sec
            conn.setReadTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


            //Send request
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(param);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                // response.append('\r');
            }
            rd.close();

            //Log.e("res", response.toString());

            responsetxt = response.toString();
            // ;


        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }


        //Log.e("UPDATE", "2");
        return responsetxt;
    }


    protected void onPostExecute(String result) {
        //Log.e("UPDATE", "Reaulted:"+result);
        delegate.processFinish(result);

    }

}

