package now.time.today;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "SQLiDB";

    // Table Names
    private static final String TABLE_TODO = "todos";

    // column names
    private static final String KEY_ID = "id";
    private static final String KEY_SENDER = "sender";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_DATE = "datee";

    // *********************************************************************************************
    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_TODO);


    }

    // Upgrading database **************************************************************************
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);


        // Create tables again
        onCreate(db);
    }

    // Creating Table TABLE_TEAM
    String CREATE_TABLE_TODO = "CREATE TABLE " + TABLE_TODO + "("
            + KEY_ID + " integer primary key autoincrement, "
            + KEY_SENDER + " text, "
            + KEY_MESSAGE + " text, "
            + KEY_DATE + " text" + ")";


    // insert values of todo
    public boolean InsertTodoDetails(String _SENDER, String _MESSAGE, String _DATE) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SENDER, _SENDER);
        contentValues.put(KEY_MESSAGE, _MESSAGE);
        contentValues.put(KEY_DATE, _DATE);


        long rowInserted = db.insert(TABLE_TODO, null, contentValues);
        db.close();

        //Log.e("here","adddeeed");

        return true;
    }


    public void _deletee(String _SENDER, String _DATE) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TODO+" WHERE "+KEY_SENDER+"='"+_SENDER+"' and "+KEY_DATE+"='"+_DATE+"'");
      //  Log.e("Delete Cat",_SENDER+" Deleted!");
    }

    // Select values of todo
    public Cursor GetAllTodoDetails() {
        SQLiteDatabase db = this.getReadableDatabase();


        String query = "SELECT * FROM " + TABLE_TODO;
        Cursor mcursor = db.rawQuery(query, null);

        if (mcursor != null) {
            mcursor.moveToFirst();
        }

        return mcursor;

    }



}
